###Create TargetGroup for ALB
resource "aws_alb_target_group" "group" {
  name     = "terraform-alb-target"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.terraform-vpc.id}"
  stickiness {
    type = "lb_cookie"
  } 
  # Alter the destination of the health check to be the login page.
  health_check {
    path = "/"
    port = 443
    interval=20
  }
}
##Register Targets
resource "aws_lb_target_group_attachment" "reg" {
  target_group_arn = aws_alb_target_group.group.arn
  target_id        = aws_instance.terraform_server1.id
  port             = 80
}
##Create ALB
resource "aws_alb" "alb" {
  name            = "terraform-alb"
  security_groups = ["${aws_security_group.alb_sg.id}"]
  subnets         = ["${aws_subnet.terraform-subnet_1.id}", "${aws_subnet.terraform-subnet_2.id}"]
  tags = {
    Name = "terraform-example-alb"
  }
}
resource "aws_alb_listener" "front_end" {
  load_balancer_arn = aws_alb.alb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:iam::785896286135:server-certificate/CSC"
  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.group.arn
  }
}
