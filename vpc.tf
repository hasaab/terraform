## Create VPC ##
resource "aws_vpc" "terraform-vpc" {
  cidr_block       = "172.16.0.0/16"
  enable_dns_hostnames = true
  tags = {
    Name = "terraform-test-vpc"
  }
}
## Create Subnet 1 ##
resource "aws_subnet" "terraform-subnet_1" {
  vpc_id     = "${aws_vpc.terraform-vpc.id}"
  cidr_block = "172.16.10.0/24"
  availability_zone = "us-west-2a"
  tags = {
    Name = "terraform-subnet_1"
  }
}
## Create Subnet 2 ##
resource "aws_subnet" "terraform-subnet_2" {
  vpc_id     = "${aws_vpc.terraform-vpc.id}"
  cidr_block = "172.16.11.0/24"
  availability_zone = "us-west-2b"

  tags = {
    Name = "terraform-subnet_2"
  }
}
##Create IGW for VPC
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.terraform-vpc.id}"

  tags = {
    Name = "terraform-vpc-igw"
  }
}
##Grant internet access on its main route table:
resource "aws_route" "route" {
  route_table_id         = "${aws_vpc.terraform-vpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.gw.id}"
}
