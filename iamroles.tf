## Defining the provider & Access and Secret access key##
##Creating Users
## Create an IAM role##
resource "aws_iam_role" "S3_Full_Access" {
  name = "S3_Full_Access"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  tags = {
      tag-key = "tag-value"
  }
}
## Create EC2 Instance Profile to link the IAM role to EC2 Instance##
resource "aws_iam_instance_profile" "EC2_profile" {
  name = "EC2_profile"
  role = "${aws_iam_role.S3_Full_Access.name}"
}
## add IAM Policies which allows EC2 instance full access to S3 Bucket ##
resource "aws_iam_role_policy" "IAM_policy" {
  name = "IAM_policy"
  role = "${aws_iam_role.S3_Full_Access.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
