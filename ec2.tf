##Create 2 EC2 instances(Webservers)
resource "aws_instance" "terraform_server1" {
    ami = "ami-0341aeea105412b57"
    instance_type = "t2.micro"
    user_data = "${file("userdatafortask610.sh")}"
    vpc_security_group_ids =  [ "${aws_security_group.ec2_sg.id}" ]
    subnet_id = "${aws_subnet.terraform-subnet_1.id}"
    key_name               = "sharedkey"
    iam_instance_profile = "${aws_iam_instance_profile.EC2_profile.name}"
    associate_public_ip_address = true
    tags = {
      Name              = "terraform_ec2_server1"
      Environment       = "development"
      Project           = "DEMO-TERRAFORM"
    }
}
resource "aws_instance" "terraform_server2" {
    ami = "ami-0341aeea105412b57"
    instance_type = "t2.micro"
    user_data = "${file("userdatafortask610.sh")}"
    vpc_security_group_ids =  [ "${aws_security_group.ec2_sg.id}" ]
    subnet_id = "${aws_subnet.terraform-subnet_2.id}"
    key_name               = "sharedkey"
    associate_public_ip_address = true
    tags = {
      Name              = "Terraform_ec2_server2"
      Environment       = "Development"
      Project           = "Project-TERRAFORM"
    }
}
